const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')

const pkg = require('./package')
const dist = path.join(__dirname, 'dist')
module.exports = {
	target: 'web',
	mode: 'none',
	entry: path.join(__dirname, pkg.main),
	output: {
		path: dist,
		filename: `${pkg.name}.js`
	},
	devtool: "source-map",
	resolve: {
		alias: {svelte: path.resolve('node_modules', 'svelte')},
		extensions: ['.mjs', '.js', '.svelte'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
	module: {
		rules: [{
			test: /\.(html|svelte)$/,
			use: {loader: 'svelte-loader'}
		},{
			test: /\.(svg|png|jpe?g)$/i,
			use: [{
				loader: 'file-loader',
				options: {name: '[name].[ext]'}
			},{
				loader: 'image-webpack-loader'
			}]
		}]
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: pkg.name.replace('-', ' ').replace(/\b\w/g, c=> c.toUpperCase()),
			favicon: 'asset/favicon.png'
		}),
	]
}
